En este documento describo cómo podemos exporta e importar datos de una tabla usando querys. 

Se utiliza la herramienta bcp.

Comando para exportar.

`>bcp "select * from tabla where columan = x" queryout <fichero.bcp> -c -S<servidor> -U<user> -P<password> -d<database-name>`

Comando para importar los datos anteriores.

`>bcp <baseDatos>.<tabla>.<columna> in <fichero.bcp> -c -S<servidor> -U<user> -P<password>`
